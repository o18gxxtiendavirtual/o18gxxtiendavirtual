
let x = 3;
let y = 4;
let z = x + y;
// alert("z = x + y = " + x + " + " + y + " = " + z)

let datos = document.getElementById("datos");

// datos.innerHTML = "x = " + x;
// datos.innerHTML += "y = " + y;
// datos.innerHTML += "z = " + z;

datos.innerHTML = "z = x + y = " + x + " + " + y + " = " + z

datos.innerHTML += ` <br><br>
    <h3> Variables </h3>
    <h2> x = ${x} </h2>
    <h2> y = ${y} </h2>
    <h2> z = ${z} </h2>   
    <br><br>
`;

// Condicional
// z = 3
let condicional =document.getElementById("condicional");
if (z < 7){
    condicional.innerHTML = `<h4> Resultado menor que 7 => z = ${z} </h4>`
} else{
    condicional.innerHTML = `<h4> Resultado mayor o igual que 7 => z = ${z} </h4>`
}


// Bucle For
let buclefor = document.getElementById("buclefor")
for (i = 0; i < z; i++){
    buclefor.innerHTML += `<h6> contador i = ${i} </h6>`
}


// Funciones
// function suma2Numeros(num1, num2){
//     let suma = document.getElementById("suma");
//     let rst = num1 + num2;
//     suma.innerHTML = `<h3>
//         suma = num1 + num2 = ${num1} + ${num2} = ${rst}
//     </h3>`;
// }

// suma2Numeros(4, 2);

// LLamado de una función a otra función
function suma2Numeros(num1, num2){
    let rst = `<h3>
             suma = num1 + num2 = ${num1} + ${num2} = ${num1 + num2}
         </h3>`;
    return rst;
}

function mostrarSuma(){
    let suma = document.getElementById("suma");
    suma.innerHTML = suma2Numeros(10, 6);
}

mostrarSuma();



// Listas
let lista = document.getElementById("lista");
let nombres = ["Hugo", "Paco", "Luis"];
for (i = 0; i < nombres.length; i++){
    lista.innerHTML += `<h4>
        Nombre[${i+1}] = ${nombres[i]}
    <h4>`;
}