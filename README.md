

Script SQL 

create database OxxGxx_Proyecto;
use	OxxGxx_Proyecto;

create table producto(
	id_prod  int not null primary key,
    nombre_prod varchar(50) null,
    precioCompra_prod decimal(16,2) null,
    precioVenta_prod decimal(16,2) null,
    cantidad_prod int not null
);

create table transaccion(
	id_trans int not null,
    tipoTrans_trans varchar(50) null,
    fecha_trans varchar(50) null,
    vendedor_trans varchar(50) null,
    comprador_trans varchar(50) null,
    total_trans decimal(16,2) null,
    constraint pk_id_trans primary key(id_trans)
);

create table detalle(
	id_det int not null primary key,
    id_prod int not null,
    id_trans int not null,
    valorDetalle_det decimal(16,2) null,
    cantidadDetalle_det int null,
    totalDetalle_det decimal(16,2) null,
    constraint fk_id_prod foreign key(id_prod) references producto(id_prod),
    constraint fk_id_trans foreign key(id_trans) references transaccion(id_trans)
);








Command line instructions

You can also upload existing files from your computer using the instructions below.

Git global setup

git config --global user.name "MisiónTIC UIS_JAS"<br>
git config --global user.email "misiontic.formador17@uis.edu.co"


Create a new repository

git clone https://gitlab.com/o18gxxtiendavirtual/o18gxxtiendavirtual.git<br>
cd o18gxxtiendavirtual<br>
git switch -c main<br>
touch README.md<br>
git add README.md<br>
git commit -m "add README"<br>
git push -u origin main


Push an existing folder

cd existing_folder<br>
git init --initial-branch=main<br>
git remote add origin https://gitlab.com/o18gxxtiendavirtual/o18gxxtiendavirtual.git<br>
git add .<br>
git commit -m "Initial commit"<br>
git push -u origin main


Push an existing Git repository

cd existing_repo<br>
git remote rename origin old-origin<br>
git remote add origin https://gitlab.com/o18gxxtiendavirtual/o18gxxtiendavirtual.git<br>
git push -u origin --all<br>
git push -u origin --tags<br>
