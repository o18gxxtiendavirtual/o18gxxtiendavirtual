package com.o18gxx.proyecto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class O18gxxProyectoApplication {

	public static void main(String[] args) {
		SpringApplication.run(O18gxxProyectoApplication.class, args);
	}

}
