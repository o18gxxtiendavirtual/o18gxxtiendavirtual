package com.o18gxx.proyecto.controlador_persistencia;

import javax.persistence.*;

// Anotaciones de Spring
@Entity // Hace entender a Java que es una clase que mapea una entidad de la base de datos
@Table(name = "producto")  // asigno el nombre debido a que es diferente de la entidad respecto a la clase
public class Producto {
    // Atributos
    @Id  // Anotación por ser la clave primaria o primary key
    // @GeneratedValue(strategy = GenerationType.IDENTITY) // En caso que el atributo de la DB, sea autoincremental.
    @Column(name = "id_prod") // teniendo en cuenta que el nombre del atributo es diferente al campo de la tabla
    private Integer idProd;

    @Column(name = "nombre_prod")
    private String nombreProd;

    private Float preciocompra_prod;
    private Float precioventa_prod;
    private Integer cantidad_prod;


    // Métodos - Funciones

    public Integer getIdProd() {
        return idProd;
    }

    public void setIdProd(Integer idProd) {
        this.idProd = idProd;
    }

    public String getNombreProd() {
        return nombreProd;
    }

    public void setNombreProd(String nombreProd) {
        this.nombreProd = nombreProd;
    }

    public Float getPreciocompra_prod() {
        return preciocompra_prod;
    }

    public void setPreciocompra_prod(Float preciocompra_prod) {
        this.preciocompra_prod = preciocompra_prod;
    }

    public Float getPrecioventa_prod() {
        return precioventa_prod;
    }

    public void setPrecioventa_prod(Float precioventa_prod) {
        this.precioventa_prod = precioventa_prod;
    }

    public Integer getCantidad_prod() {
        return cantidad_prod;
    }

    public void setCantidad_prod(Integer cantidad_prod) {
        this.cantidad_prod = cantidad_prod;
    }

}
