package com.o18gxx.proyecto.controlador_persistencia;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "transaccion")
public class Transaccion {

    // Atributos
    @Id
    private Integer id_trans;

    private String tipoTrans_trans;
    private String fecha_trans;
    private String vendedor_trans;
    private String comprador_trans;
    private Float total_trans;

    // Métodos - Funciones

    public Integer getId_trans() {
        return id_trans;
    }

    public void setId_trans(Integer id_trans) {
        this.id_trans = id_trans;
    }

    public String getTipoTrans_trans() {
        return tipoTrans_trans;
    }

    public void setTipoTrans_trans(String tipoTrans_trans) {
        this.tipoTrans_trans = tipoTrans_trans;
    }

    public String getFecha_trans() {
        return fecha_trans;
    }

    public void setFecha_trans(String fecha_trans) {
        this.fecha_trans = fecha_trans;
    }

    public String getVendedor_trans() {
        return vendedor_trans;
    }

    public void setVendedor_trans(String vendedor_trans) {
        this.vendedor_trans = vendedor_trans;
    }

    public String getComprador_trans() {
        return comprador_trans;
    }

    public void setComprador_trans(String comprador_trans) {
        this.comprador_trans = comprador_trans;
    }

    public Float getTotal_trans() {
        return total_trans;
    }

    public void setTotal_trans(Float total_trans) {
        this.total_trans = total_trans;
    }
}
