package com.o18gxx.proyecto.controlador_persistencia.repository_service;

import com.o18gxx.proyecto.controlador_persistencia.Producto;
import com.o18gxx.proyecto.modelo_logica.ProductoCrudRepositoryDao;
import com.o18gxx.proyecto.modelo_logica.repository_service.ProductoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProductoServiceImplement implements ProductoServicio {
    // Atributo
    @Autowired // permite conectar y utilizar el repositorio desde la clase
    private ProductoCrudRepositoryDao productoCrudRepositoryDao;

    // Métodos o Funciones
    @Override
    @Transactional(readOnly = false)
    public Producto save(Producto producto){
        return productoCrudRepositoryDao.save(producto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Producto> findAll(){
        return (List<Producto>) productoCrudRepositoryDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Producto findById(Integer idProd){
        return productoCrudRepositoryDao.findById(idProd).orElse(null);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer idProd){
        productoCrudRepositoryDao.deleteById(idProd);
    }

}
