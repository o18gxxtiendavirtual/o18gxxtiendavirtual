package com.o18gxx.proyecto.modelo_logica;

import com.o18gxx.proyecto.controlador_persistencia.Detalle;
import org.springframework.data.repository.CrudRepository;

public interface DetalleCrudRepositoryDao extends CrudRepository<Detalle, Integer> {
}
