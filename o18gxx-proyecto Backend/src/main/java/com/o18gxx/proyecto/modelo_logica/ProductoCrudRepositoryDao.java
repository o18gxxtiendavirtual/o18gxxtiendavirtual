package com.o18gxx.proyecto.modelo_logica;

import com.o18gxx.proyecto.controlador_persistencia.Producto;
//import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProductoCrudRepositoryDao extends CrudRepository<Producto, Integer> {

    // Nueva forma en el manejo de Spring  Data
    //public List<Producto> findAll();

    // Forma Anterior
    //@Query(value = "SELECT * FROM producto", nativeQuery = true)
}
