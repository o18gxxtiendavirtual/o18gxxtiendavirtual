package com.o18gxx.proyecto.vista_presentacion;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/saludo")  // responde bajo la ruta saludo
public class HolaMuldoControler {

    @GetMapping("/hola")  // Responde al método bajo anotación Get.
    public String saludar(){
        return "<br> <h1>Hola Tripulantes</h1>... <br> <h3>Primer ejemplo con Spring Boot...</h3>";
    }
}
