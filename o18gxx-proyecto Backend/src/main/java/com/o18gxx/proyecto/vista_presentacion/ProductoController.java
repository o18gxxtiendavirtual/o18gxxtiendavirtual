package com.o18gxx.proyecto.vista_presentacion;

import com.o18gxx.proyecto.controlador_persistencia.Producto;
import com.o18gxx.proyecto.controlador_persistencia.repository_service.ProductoServiceImplement;
import com.o18gxx.proyecto.modelo_logica.repository_service.ProductoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/producto")
public class ProductoController {
    // Atributos
    @Autowired // logra la conexión y utiliza el repositorio desde la clase
    private ProductoServicio productoServicio;

    // Métodos - Funciones  del CRUD

    // Create
    @PutMapping(value = "/")
    public ResponseEntity<Producto> agregar(@RequestBody Producto producto){
        Producto obj = productoServicio.save(producto);
        return new ResponseEntity<Producto>(obj, HttpStatus.OK);
    }

    // Read
    @GetMapping("/list")
    public List<Producto> consultarTodo(){
        return productoServicio.findAll();
    }

    @GetMapping("/list/{id}")
    public Producto consultarPorId(@PathVariable("id") Integer id){
        return productoServicio.findById(id);
    }

    // Update
    @PostMapping(value = "/")
    public ResponseEntity<Producto> editar(@RequestBody Producto producto){
        Producto obj = productoServicio.findById(producto.getIdProd());
        if (obj != null){
            obj.setNombreProd(producto.getNombreProd());
            obj.setPreciocompra_prod(producto.getPreciocompra_prod());
            obj.setPrecioventa_prod(producto.getPrecioventa_prod());
            obj.setCantidad_prod(producto.getCantidad_prod());
            productoServicio.save(obj);
            return new ResponseEntity<Producto>(obj, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<Producto>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Producto> eliminar(@PathVariable("id") Integer idProd){
        Producto obj = productoServicio.findById(idProd);
        if (obj != null){
            productoServicio.delete(idProd);
            return new ResponseEntity<Producto>(obj, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<Producto>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
